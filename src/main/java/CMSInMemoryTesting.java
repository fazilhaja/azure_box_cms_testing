import java.io.*;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
import java.util.*;
//import java.util.Arrays;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
//import net.jpountz.xxhash.XXHash32;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.commands.ProtocolCommand;
//import redis.clients.jedis.util.SafeEncoder;




class CountMinSketch implements Serializable{
    public static final long PRIME_MODULUS = (1L << 31) - 1;
    int depth;
    int width;
    long[][] table;
    long[] hashA;
    long size;
    double eps;
    double confidence;

    public CountMinSketch(int depth, int width, int seed) {
        this.depth = depth;
        this.width = width;
        this.eps = 2.0 / width;
        this.confidence = 1 - 1 / Math.pow(2, depth);
        initTablesWith(depth, width, seed);
    }

    public CountMinSketch() {

    }

    private void initTablesWith(int depth, int width, int seed) {
        this.table = new long[depth][width];
        this.hashA = new long[depth];
        Random r = new Random(seed);
        // We're using a linear hash functions
        // of the form (a*x+b) mod p.
        // a,b are chosen independently for each hash function.
        // However we can set b = 0 as all it does is shift the results
        // without compromising their uniformity or independence with
        // the other hashes.
        for (int i = 0; i < depth; ++i) {
            hashA[i] = r.nextInt(Integer.MAX_VALUE);
        }
    }


    int hash(long item, int i) {
        long hash = hashA[i] * item;
        // A super fast way of computing x mod 2^p-1
        // See http://www.cs.princeton.edu/courses/archive/fall09/cos521/Handouts/universalclasses.pdf
        // page 149, right after Proposition 7.
        hash += hash >> 32;
        hash &= PRIME_MODULUS;
        // Doing "%" after (int) conversion is ~2x faster than %'ing longs.
        return ((int) hash) % width;
    }
    private static void checkSizeAfterOperation(long previousSize, String operation, long newSize) {
        if (newSize < previousSize) {
           /* throw new IllegalStateException("Overflow error: the size after calling `" + operation +
                    "` is smaller than the previous size. " +
                    "Previous size: " + previousSize +
                    ", New size: " + newSize);
                */
            System.out.println("OverFlow error");
        }
    }

    private void checkSizeAfterAdd(String item, long count) {
        long previousSize = size;
        size += count;
        checkSizeAfterOperation(previousSize, "add(" + item + "," + count + ")", size);
    }

    public void add(long item, long count) {
        if (count < 0) {
            // Actually for negative increments we'll need to use the median
            // instead of minimum, and accuracy will suffer somewhat.
            // Probably makes sense to add an "allow negative increments"
            // parameter to constructor.
            //throw new IllegalArgumentException("Negative increments not implemented");
            System.out.println("Negative increments");
        }
        for (int i = 0; i < depth; ++i) {
            table[i][hash(item, i)] += count;
        }

        checkSizeAfterAdd(String.valueOf(item), count);
    }

    public long estimateCount(long item) {
        long res = Long.MAX_VALUE;
        for (int i = 0; i < depth; ++i) {
            res = Math.min(res, table[i][hash(item, i)]);
        }
        return res;
    }

    private static int[] getHashBuckets(String key,int hashCount,int max) {

        int[] result = new int[hashCount];
        byte[] b = key.getBytes();
        int hash1 = XXHash.hash32(b,0,b.length,0);
        int hash2 = XXHash.hash32(b,0,b.length,hash1);

        for(int i = 0 ; i < hashCount ; i++) {
            result[i] = Math.abs((hash1 + i * hash2) % max);

        }

        return result;




    }

    public void add(String item, long count) {
        if (count < 0) {
            // Actually for negative increments we'll need to use the median
            // instead of minimum, and accuracy will suffer somewhat.
            // Probably makes sense to add an "allow negative increments"
            // parameter to constructor.
            throw new IllegalArgumentException("Negative increments not implemented");
        }
        int[] buckets = getHashBuckets(item, depth, width);
        for (int i = 0; i < depth; ++i) {
            table[i][buckets[i]] += count;
        }

        checkSizeAfterAdd(item, count);
    }

    public long estimateCount(String item) {
        long res = Long.MAX_VALUE;
        int[] buckets = getHashBuckets(item, depth, width);
        for (int i = 0; i < depth; ++i) {
            res = Math.min(res, table[i][buckets[i]]);
        }
        return res;
    }

    public void printTable() {
        for(int i = 0 ; i < depth ; i++)
        {
            System.out.println("Row "+i+" :  " );
            for(int j = 0 ; j < width ; j++)
            {
                System.out.print(table[i][j] + "    ");
            }
            System.out.println("\n");
        }
    }

    public static byte[] serialize(CountMinSketch sketch) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream s = new DataOutputStream(bos);
        try {
            s.writeLong(sketch.size);
            s.writeInt(sketch.depth);
            s.writeInt(sketch.width);
            for (int i = 0; i < sketch.depth; ++i) {
                s.writeLong(sketch.hashA[i]);
                for (int j = 0; j < sketch.width; ++j) {
                    s.writeLong(sketch.table[i][j]);
                }
            }
            return bos.toByteArray();
        } catch (IOException e) {
            // Shouldn't happen
            throw new RuntimeException(e);
        }
    }

    public static CountMinSketch deserialize(byte[] data) {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        DataInputStream s = new DataInputStream(bis);
        try {
            CountMinSketch sketch = new CountMinSketch();
            sketch.size = s.readLong();
            sketch.depth = s.readInt();
            sketch.width = s.readInt();
            sketch.eps = 2.0 / sketch.width;
            sketch.confidence = 1 - 1 / Math.pow(2, sketch.depth);
            sketch.hashA = new long[sketch.depth];
            sketch.table = new long[sketch.depth][sketch.width];
            for (int i = 0; i < sketch.depth; ++i) {
                sketch.hashA[i] = s.readLong();
                for (int j = 0; j < sketch.width; ++j) {
                    sketch.table[i][j] = s.readLong();
                }
            }
            return sketch;
        } catch (IOException e) {
            // Shouldn't happen
            throw new RuntimeException(e);
        }
    }






}

public class CMSInMemoryTesting {
    /*
    private enum Command implements ProtocolCommand {
        INCRBY("CMS.INCRBY"),
        MERGE("CMS.MERGE"),
        QUERY("CMS.QUERY"),
        INITBYERR("CMS.INITBYERR"),
        INITBYDIM("CMS.INITBYDIM");
        private final byte[] raw;

        Command(String alt) {
            raw = SafeEncoder.encode(alt);
        }

        public byte[] getRaw() {
            return raw;
        }
    }
    */
    public static void main(String []args) {
        CountMinSketch inmemcms = new CountMinSketch(54,8000000,30);
        String[] Words = new String[17 * 1000000];
        Multiset<String> mset = HashMultiset.create();
        try {
            int i = 0;
            FileReader reader = new FileReader("Words1.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Words[i] = line;
                i++;
                //mset.add(line);


            }
            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(Words.length);
        System.out.println(mset.size());
        long iter = 0;
        //Jedis jedis = new Jedis("127.0.0.1", 6380);
        while (iter < 5 * 3000000) {
            Random random = new Random();
            int index = random.nextInt(3000000);
            mset.add(Words[index]);
            List<byte[]> args1 = new ArrayList(3);
//            args1.add(SafeEncoder.encode("Complexity2"));
//            args1.add(SafeEncoder.encode(Words[index]));
//            args1.add(SafeEncoder.encode("1"));
            //jedis.getClient().sendCommand(Command.INCRBY, args1.toArray(new byte[args1.size()][]));
            inmemcms.add(Words[index],1);


            /*
            if(mset.count(Words[index]) > 5) {
                System.out.println(""+Words[index] + "   " + mset.count(Words[index]));
            }
            */
            if(iter % 1000000 == 0)
                System.out.println("Iteration " + iter);

            iter++;
        }

       // jedis.close();
        //jedis = new Jedis("127.0.0.1", 6380);

        System.out.println("Completed1");
        long same = 0;
        long inmemsame = 0;
        double diff = 0;
        double inmemdiff = 0;
        int maxDiff = 0;
        long inmemMaxDiff = 0;
        int zeroCount = 0;
        int inmemZeroCount = 0;
        Multiset<Integer> countOfDiffs = HashMultiset.create();
        Multiset<Long> inmemCountOfDiffs = HashMultiset.create();

        for (int i = 0; i < 3000000; i++) {
            List<byte[]> args2 = new ArrayList(2);
            //args2.add(SafeEncoder.encode("Complexity2"));
            //args2.add(SafeEncoder.encode(Words[i]));
            /*jedis.getClient().sendCommand(Command.QUERY, args2.toArray(new byte[args2.size()][]));
            List<byte[]> s = jedis.getClient().getBinaryMultiBulkReply();
            ListIterator<byte[]> s1 = s.listIterator();
            //String token =  s1.next().toString();
            String s2 = s.toString();
            String s3 = s2.substring(1, s2.length() - 1);
            int result = Integer.parseInt(s3);
            */
            long inmemresult = inmemcms.estimateCount(Words[i]);
            //System.out.println(s3);
            if(i % 1000000 == 0) {
                System.out.println("i = " + i);
            }
            /*

            if (result != mset.count(Words[i]) && mset.count(Words[i]) != 0) {
                //System.out.println(Words[i] + "   mset count :" + mset.count(Words[i]) + "    redis count " + result);
                diff += (100 * (Math.abs(result - mset.count(Words[i]))) * 1.0 / (1 + mset.count(Words[i]) ));
                if (maxDiff < Math.abs(result - mset.count(Words[i])) ) {
                    maxDiff = Math.abs(result - mset.count(Words[i]));

                }
                countOfDiffs.add(result - mset.count(Words[i]));
            }

            else if (mset.count(Words[i]) == 0){

                diff += (100 * result);

                if(maxDiff < result) {
                    maxDiff = result;
                }
                countOfDiffs.add(result);


                Double check = new Double(diff);
                if(check.isNaN() )
                {
                    System.out.println(Words[i] + "    mset count " + mset.count(Words[i]) + "redis Count   " + result);
                }
                zeroCount++;



            }
            else {
                same++;
                countOfDiffs.add(0);
            }
            */
            //Newly added
            if (inmemresult != mset.count(Words[i]) && mset.count(Words[i]) != 0) {
                inmemdiff += (100 * (Math.abs(inmemresult - mset.count(Words[i]))) * 1.0 / (1 + mset.count(Words[i]) ));
                if (inmemMaxDiff < Math.abs(inmemresult - mset.count(Words[i]))) {
                    inmemMaxDiff = Math.abs(inmemresult - mset.count(Words[i]));
                }
                inmemCountOfDiffs.add((inmemresult - mset.count(Words[i])));
            }
            else if (mset.count(Words[i]) == 0 ) {

                inmemdiff += (100 * inmemresult);

                if(inmemMaxDiff < inmemresult) {
                    inmemMaxDiff = inmemresult;
                }
                inmemCountOfDiffs.add(inmemresult);
                inmemZeroCount++;
            }
            else {
                inmemsame++;
            }

        }
        //jedis.close();
        //System.out.println("Completed  2 \n Redis Results");
        //System.out.println("Same Count " + same);
        System.out.println("Multi Set Size  " + mset.size());
        //System.out.println("Diff " + diff);
        //System.out.println("Diff / 17mn   " + diff / (3000000));
        //System.out.println("Max Diff " + maxDiff);
        //System.out.println("Zero Count " + zeroCount);
        //System.out.println("Statistics of the frequencies ");
        //Iterator<Integer> it = countOfDiffs.iterator();
        /*int it = -maxDiff;
        while(it <= maxDiff) {
            System.out.println(it + ".............." + countOfDiffs.count(it));
            it++;
        }*/

        System.out.println("In Memory Results");
        System.out.println("Same Count " + inmemsame);
        System.out.println("Diff " + inmemdiff);
        System.out.println("Diff / 17mn   " + inmemdiff / (3000000));
        System.out.println("Max Diff " + inmemMaxDiff);
        System.out.println("Zero Count " + inmemZeroCount);
        System.out.println("Statistics of the frequencies ");
        //Iterator<Integer> it = countOfDiffs.iterator();
        long it1 = -inmemMaxDiff;
        while(it1 <= inmemMaxDiff) {
            System.out.println(it1 + ".............." + inmemCountOfDiffs.count(it1));
            it1++;
        }

    }


}
