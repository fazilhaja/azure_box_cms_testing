
import java.io.*;
import java.util.*;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
//import jdk.nashorn.internal.runtime.WithObject;
//import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
//import redis.clients.jedis.HostAndPort;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisSentinelPool;
//import redis.clients.jedis.commands.ProtocolCommand;
//import redis.clients.jedis.exceptions.JedisException;
//import redis.clients.jedis.util.SafeEncoder;

public class CmsHandler {
//    private enum Command implements ProtocolCommand {
//        INCRBY("CMS.INCRBY"),
//        MERGE("CMS.MERGE"),
//        QUERY("CMS.QUERY"),
//        INITBYERR("CMS.INITBYERR"),
//        INITBYDIM("CMS.INITBYDIM");
//        private final byte[] raw;
//
//        Command(String alt) {
//            raw = SafeEncoder.encode(alt);
//        }
//
//        public byte[] getRaw() {
//            return raw;
//        }
//    }

    private static void assertReplyNotError(final String str) {
        if (str.startsWith("-ERR"))
            throw new RuntimeException(str.substring(5));
    }

    private static void assertReplyOK(final String str) {
        if (!str.equals("OK"))
            throw new RuntimeException(str);
    }


    public static void main(String[] args) {
//
// /*       System.out.println("Hello World!");
//        Set<String> s = new HashSet<String>();
//        s.add(new HostAndPort("127.0.0.1",5001).toString());
//        s.add(new HostAndPort("127.0.0.1",5002).toString());
//        s.add(new HostAndPort("127.0.0.1",5003).toString());
//
//
//
//
//
//
//        JedisSentinelPool pool = new JedisSentinelPool("mymaster",s);
//        Jedis jedis = null;
//
//        System.out.println("Pool Connected");
//
//        try {
//            jedis = pool.getResource();
//
//            jedis.getClient().sendCommand(Command.INCRBY,"'haja' 'hello' 100 ");
//            String status = jedis.getClient().getStatusCodeReply();
//            jedis.close();
//
//            System.out.println(status);
//
//
//
//        } catch (JedisException e) {
//            System.out.println(e);
//        }
//*/
///*
//
//        List<byte[]> args1 = new ArrayList(3);
//        args1.add(SafeEncoder.encode("haja"));
//        args1.add(SafeEncoder.encode("done"));
//        args1.add(SafeEncoder.encode("100"));
//        Jedis jedis = new Jedis("127.0.0.1",6380);
//
//        jedis.getClient().sendCommand(Command.INCRBY,args1.toArray(new byte[args1.size()][]));
//        String status = jedis.getClient().getStatusCodeReply();
//        System.out.println(status);
//        jedis.close();
//
//
//       Jedis jedis = new Jedis("127.0.0.1",6380);
//
//        List<byte[]> args2 = new ArrayList(2);
//        args2.add(SafeEncoder.encode("haja"));
//        args2.add(SafeEncoder.encode("done"));
//        args2.add(SafeEncoder.encode("hello"));
//        jedis.getClient().sendCommand(Command.QUERY,args2.toArray(new byte[args2.size()][]));
//        List<byte[]> s = jedis.getClient().getBinaryMultiBulkReply();
//
//        //String status = jedis.getClient().getStatusCodeReply();
//        //System.out.println(status);
//        ListIterator<byte[]> s1 = s.listIterator();
//        System.out.println(s1.next());
//        System.out.println(s1.next());
//        */
//
//        char[] chars = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
//        /*
//        Set<String> WordDict = new HashSet<String>();
//        while (WordDict.size() < 17 * 1000000) {
//            StringBuilder sb = new StringBuilder(20);
//            Random random = new Random();
//            for (int i = 0; i < 20; i++) {
//                char c = chars[random.nextInt(chars.length)];
//                sb.append(c);
//            }
//            WordDict.add(sb.toString());
//            if(WordDict.size() % 1000000 == 0 ) {
//                System.out.println("Word Dict Size " + WordDict.size());
//            }
//
//
//        }
//        Iterator<String> it = WordDict.iterator();
//
//        try {
//            FileWriter out = new FileWriter("Words1.txt");
//            while (it.hasNext()) {
//                out.write(it.next());
//                out.write("\r\n");
//
//            }
//            out.close();
//
//
//
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        */
//
//
//        String[] Words = new String[17 * 1000000];
//        Multiset<String> mset = HashMultiset.create();
//        try {
//            int i = 0;
//            FileReader reader = new FileReader("Words1.txt");
//            BufferedReader bufferedReader = new BufferedReader(reader);
//            String line;
//            while ((line = bufferedReader.readLine()) != null) {
//                Words[i] = line;
//                i++;
//                //mset.add(line);
//
//
//            }
//            reader.close();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println(Words.length);
//        System.out.println(mset.size());
//        long iter = 0;
//        Jedis jedis = new Jedis("127.0.0.1", 6380);
//        while (iter < 5 * 3000000) {
//            Random random = new Random();
//            int index = random.nextInt(3000000);
//            mset.add(Words[index]);
//            List<byte[]> args1 = new ArrayList(3);
//            args1.add(SafeEncoder.encode("Complexity2"));
//            args1.add(SafeEncoder.encode(Words[index]));
//            args1.add(SafeEncoder.encode("1"));
//            jedis.getClient().sendCommand(Command.INCRBY, args1.toArray(new byte[args1.size()][]));
//
//
//
//            /*
//            if(mset.count(Words[index]) > 5) {
//                System.out.println(""+Words[index] + "   " + mset.count(Words[index]));
//            }
//            */
//            if(iter % 1000000 == 0)
//                System.out.println("Iteration " + iter);
//
//            iter++;
//        }
//
//        jedis.close();
//        jedis = new Jedis("127.0.0.1", 6380);
//
//        System.out.println("Completed1");
//        long same = 0;
//        double diff = 0;
//        int maxDiff = 0;
//        int zeroCount = 0;
//        Multiset<Integer> countOfDiffs = HashMultiset.create();
//
//        for (int i = 0; i < 3000000; i++) {
//            List<byte[]> args2 = new ArrayList(2);
//            args2.add(SafeEncoder.encode("Complexity2"));
//            args2.add(SafeEncoder.encode(Words[i]));
//            jedis.getClient().sendCommand(Command.QUERY, args2.toArray(new byte[args2.size()][]));
//            List<byte[]> s = jedis.getClient().getBinaryMultiBulkReply();
//            ListIterator<byte[]> s1 = s.listIterator();
//            //String token =  s1.next().toString();
//            String s2 = s.toString();
//            String s3 = s2.substring(1, s2.length() - 1);
//            int result = Integer.parseInt(s3);
//            //System.out.println(s3);
//            if(i % 1000000 == 0) {
//                System.out.println("i = " + i);
//            }
//
//            if (result != mset.count(Words[i]) && mset.count(Words[i]) != 0) {
//                //System.out.println(Words[i] + "   mset count :" + mset.count(Words[i]) + "    redis count " + result);
//                diff += (100 * (Math.abs(result - mset.count(Words[i]))) * 1.0 / (1 + mset.count(Words[i]) ));
//                if (maxDiff < Math.abs(result - mset.count(Words[i])) ) {
//                    maxDiff = Math.abs(result - mset.count(Words[i]));
//
//                }
//                countOfDiffs.add(result - mset.count(Words[i]));
//            }
//            else if (mset.count(Words[i]) == 0){
//
//                diff += (100 * result);
//
//                if(maxDiff < result) {
//                    maxDiff = result;
//                }
//                countOfDiffs.add(result);
//
//
//                Double check = new Double(diff);
//                if(check.isNaN() )
//                {
//                    System.out.println(Words[i] + "    mset count " + mset.count(Words[i]) + "redis Count   " + result);
//                }
//                zeroCount++;
//
//
//
//            }
//            else {
//                same++;
//                countOfDiffs.add(0);
//            }
//
//        }
//        jedis.close();
//        System.out.println("Completed  2");
//        System.out.println("Same Count " + same);
//        System.out.println("Diff " + diff);
//        System.out.println("Diff / 17mn   " + diff / (3000000));
//        System.out.println("Max Diff " + maxDiff);
//        System.out.println("Zero Count " + zeroCount);
//        System.out.println("Statistics of the frequencies ");
//        //Iterator<Integer> it = countOfDiffs.iterator();
//        int it = -maxDiff;
//        while(it <= maxDiff) {
//            System.out.println(it + ".............." + countOfDiffs.count(it));
//            it++;
//        }
//
//
//        //System.out.println(WordDict.size());
//        // Multiset<String> mset = HashMultiset.create(WordDict);
//        //WordDict = new HashSet<String>();
//
//
// /*       System.out.println(mset.size());
//        Iterator<String> it = mset.iterator();
//        for(int i = 0; i < 10 ; i++) {
//            String st = it.next();
//            System.out.println(st);
//            mset.add(st);
//            System.out.println(mset.count(st));
//        }
//     */
//    /*
//        long iter = 0;
//        while (iter < 4 * 100000) {
//            StringBuilder sb = new StringBuilder(20);
//            Random random = new Random();
//            for (int i = 0; i < 20; i++) {
//                char c = chars[random.nextInt(chars.length)];
//                sb.append(c);
//            }
//            if (mset.contains(sb.toString())) {
//                iter++;
//                mset.add(sb.toString());
//                System.out.println(sb.toString());
//                System.out.println(mset.count(sb.toString()));
//                if(mset.count(sb.toString()) > 5) {
//                    System.out.println(sb.toString());
//                }
//            }
//
//        }
//        System.out.println("Completed");
//
//    */
//
//
   }


}
